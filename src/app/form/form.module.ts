import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Form, FormSchema } from './form.model';
import { FormService } from './form.service';
import { FormResolver } from './form.resolver';
import { ExpenditureModule } from '../expenditure/expenditure.module';
import { EmployeeModule } from '../employee/employee.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Form.name, schema: FormSchema }]),
    ExpenditureModule,
    EmployeeModule,
  ],
  providers: [FormService, FormResolver],
})
export class FormModule {}
