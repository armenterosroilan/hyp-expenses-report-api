import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Expenditure } from '../expenditure/expenditure.model';
import { Employee } from '../employee/employee.model';

@ObjectType()
@Schema()
export class Form extends Document {
  @Field(() => ID)
  _id: string;

  @Field()
  @Prop()
  name: string;

  @Field()
  @Prop()
  concept: string;

  @Field()
  @Prop()
  startDate: Date;

  @Field()
  @Prop()
  endDate: Date;

  @Field()
  @Prop()
  total: number;

  @Field()
  @Prop()
  accepted: string;

  @Field()
  @Prop()
  signBase64: string;

  @Field(() => String)
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: () => Employee })
  employee: string;

  @Field(() => [String])
  @Prop({ type: [MongooseSchema.Types.ObjectId], ref: () => Expenditure })
  expenses: string[];
}
export type FormFilter = {
  _id: string;
  name: string;
};
export const FormSchema = SchemaFactory.createForClass(Form);
