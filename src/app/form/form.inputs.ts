import { Field, InputType } from '@nestjs/graphql';
import { FilterQuery, Schema as MongooseSchema } from 'mongoose';
import { Form, FormFilter } from './form.model';

@InputType()
export class CreateFormInput {
  @Field(() => String)
  name: string;

  @Field(() => String)
  concept: string;

  @Field(() => Date)
  startDate: Date;

  @Field(() => Date)
  endDate: Date;

  @Field(() => Number)
  total: number;

  @Field(() => String)
  accepted: string;

  @Field(() => String)
  signBase64: string;

  @Field(() => String)
  employee: string;

  @Field(() => [String])
  expenses: string[];
}

@InputType()
export class ListFormInput implements FilterQuery<FormFilter> {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  concept?: string;

  @Field(() => Date, { nullable: true })
  startDate?: Date;

  @Field(() => Date, { nullable: true })
  endDate?: Date;

  @Field(() => Number, { nullable: true })
  total?: number;

  @Field(() => String, { nullable: true })
  accepted?: string;

  @Field(() => String, { nullable: true })
  signBase64?: string;
}

@InputType()
export class UpdateFormInput {
  @Field(() => String)
  _id: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  concept?: string;

  @Field(() => Date, { nullable: true })
  startDate?: Date;

  @Field(() => Date, { nullable: true })
  endDate?: Date;

  @Field(() => Number, { nullable: true })
  total?: number;

  @Field(() => String, { nullable: true })
  accepted?: string;

  @Field(() => String, { nullable: true })
  signBase64?: string;

  @Field(() => String, { nullable: true })
  employee?: string;

  @Field(() => [String], { nullable: true })
  expenses?: string[];
}
