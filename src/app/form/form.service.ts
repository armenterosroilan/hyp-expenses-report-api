import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Schema as MongooseSchema } from 'mongoose';

import { Form, FormFilter } from './form.model';
import { CreateFormInput, ListFormInput, UpdateFormInput } from './form.inputs';
import { Expenditure } from '../expenditure/expenditure.model';

@Injectable()
export class FormService {
  constructor(@InjectModel(Form.name) private formModel: Model<Form>) {}
  async create(payload: CreateFormInput) {
    const createdForm = new this.formModel(payload);
    return await createdForm.save();
  }
  async getById(_id: string) {
    return this.formModel.findById(_id).lean();
  }
  async list(filters: FilterQuery<FormFilter>) {
    return this.formModel.find(filters).lean();
  }
  async update(payload: UpdateFormInput) {
    return this.formModel
      .findByIdAndUpdate(payload._id, payload, { new: true })
      .lean();
  }
  async delete(_id: string) {
    return this.formModel.findByIdAndDelete(_id).lean();
  }
}
