import {
  Args,
  Mutation,
  Query,
  Resolver,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { Schema as MongooseSchema } from 'mongoose';

import { Form } from './form.model';
import { Expenditure } from '../expenditure/expenditure.model';
import { FormService } from './form.service';
import { CreateFormInput, UpdateFormInput, ListFormInput } from './form.inputs';
import { ExpenditureService } from '../expenditure/expenditure.service';
import { EmployeeService } from '../employee/employee.service';
import { Employee } from '../employee/employee.model';

@Resolver(() => Form)
export class FormResolver {
  constructor(
    private formService: FormService,
    private expeditureService: ExpenditureService,
    private employeeService: EmployeeService,
  ) {}

  @Query(() => Form)
  async form(@Args('_id', { type: () => String }) _id: string) {
    return this.formService.getById(_id);
  }

  @Query(() => [Form])
  async forms(
    @Args('filters', { nullable: true }) filters: ListFormInput = {},
  ) {
    return this.formService.list(filters);
  }

  @Mutation(() => Form)
  async createForm(@Args('payload') payload: CreateFormInput) {
    return this.formService.create(payload);
  }

  @Mutation(() => Form)
  async updateForm(@Args('payload') payload: UpdateFormInput) {
    return this.formService.update(payload);
  }

  @Mutation(() => Form)
  async deleteForm(@Args('_id', { type: () => String }) _id: string) {
    return this.formService.delete(_id);
  }

  @ResolveField(() => [Expenditure])
  async Allexpenses(@Parent() form: Form) {
    return this.expeditureService.list({ _id: { $in: form.expenses } });
  }

  @ResolveField(() => Employee)
  async employee(@Parent() form: Form) {
    return this.employeeService.list({ _id: form.employee });
  }
}
