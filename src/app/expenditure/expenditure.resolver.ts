import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Schema as MongooseSchema } from 'mongoose';

import { Expenditure } from './expenditure.model';
import {
  CreateExpenditureInput,
  UpdateExpenditureInput,
  ListExpenditureInput,
} from './expenditure.inputs';
import { ExpenditureService } from './expenditure.service';

@Resolver(() => Expenditure)
export class ExpenditureResolver {
  constructor(private expenditureService: ExpenditureService) {}

  @Query(() => Expenditure)
  async expenditure(
    @Args('_id', { type: () => String }) _id: string,
  ) {
    return this.expenditureService.getById(_id);
  }

  @Query(() => [Expenditure])
  async expenses(
    @Args('filters', { nullable: true }) filters: ListExpenditureInput = {},
  ) {
    return this.expenditureService.list(filters);
  }

  @Mutation(() => Expenditure)
  async createExpenditure(@Args('payload') payload: CreateExpenditureInput) {
    return this.expenditureService.create(payload);
  }

  @Mutation(() => Expenditure)
  async updateExpenditure(@Args('payload') payload: UpdateExpenditureInput) {
    return this.expenditureService.update(payload);
  }

  @Mutation(() => Expenditure)
  async deleteExpenditure(
    @Args('_id', { type: () => String }) _id: string,
  ) {
    return this.expenditureService.delete(_id);
  }
}
