import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Schema as MongooseSchema } from 'mongoose';

import { ExpendFilter, Expenditure } from './expenditure.model';
import {
  CreateExpenditureInput,
  ListExpenditureInput,
  UpdateExpenditureInput,
} from './expenditure.inputs';

@Injectable()
export class ExpenditureService {
  constructor(
    @InjectModel(Expenditure.name) private expenditureModel: Model<Expenditure>,
  ) {}
  create(payload: CreateExpenditureInput) {
    const createdExpenditure = new this.expenditureModel(payload);
    return createdExpenditure.save();
  }
  getById(_id: string) {
    return this.expenditureModel.findById(_id).lean();
  }
  list(filters: FilterQuery<ExpendFilter>) {
    return this.expenditureModel.find(filters).lean();
  }
  update(payload: UpdateExpenditureInput) {
    return this.expenditureModel
      .findByIdAndUpdate(payload._id, {}, { new: true })
      .lean();
  }
  delete(_id: string) {
    return this.expenditureModel.findByIdAndDelete(_id).lean();
  }
}
