import { Document, Schema as MongooseSchema } from 'mongoose';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@ObjectType()
@Schema()
export class Expenditure extends Document {
  @Field(() => ID)
  _id: string;

  @Field()
  @Prop()
  name: string;

  @Field()
  @Prop()
  date: Date;

  @Field()
  @Prop()
  account: string;

  @Field()
  @Prop()
  description: string;

  @Field()
  @Prop()
  total: number;
}

export type ExpendFilter = {
  _id: string;
  name: string;
};
export const ExpenditureSchema = SchemaFactory.createForClass(Expenditure);
