import { FilterQuery, Schema as MongooseSchema } from 'mongoose';
import { Field, Float, InputType } from '@nestjs/graphql';
import { ExpendFilter, Expenditure } from './expenditure.model';

@InputType()
export class CreateExpenditureInput {
  @Field(() => String)
  name: string;
  @Field(() => Date)
  date: Date;
  @Field(() => String)
  account: string;
  @Field(() => String)
  description: string;
  @Field(() => Number)
  total: number;
}

@InputType()
export class ListExpenditureInput implements FilterQuery<ExpendFilter> {
  @Field(() => String, { nullable: true })
  _id?: string;
  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => Date, { nullable: true })
  date?: Date;
  @Field(() => String, { nullable: true })
  account?: string;
  @Field(() => String, { nullable: true })
  description?: string;
  @Field(() => Number, { nullable: true })
  total?: number;
}

@InputType()
export class UpdateExpenditureInput {
  @Field(() => String)
  _id: string;

  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => Date, { nullable: true })
  date?: Date;
  @Field(() => String, { nullable: true })
  account?: string;
  @Field(() => String, { nullable: true })
  description?: string;
  @Field(() => Number, { nullable: true })
  total?: number;
}
