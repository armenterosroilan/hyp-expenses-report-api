import { Test, TestingModule } from '@nestjs/testing';
import { ExpenditureResolver } from './expenditure.resolver';

describe('ExpenditureResolver', () => {
  let resolver: ExpenditureResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExpenditureResolver],
    }).compile();

    resolver = module.get<ExpenditureResolver>(ExpenditureResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
