import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Expenditure, ExpenditureSchema } from './expenditure.model';
import { ExpenditureService } from './expenditure.service';
import { ExpenditureResolver } from './expenditure.resolver';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Expenditure.name, schema: ExpenditureSchema },
    ]),
  ],
  providers: [ExpenditureService, ExpenditureResolver],
  exports: [ ExpenditureService ]
})
export class ExpenditureModule {}
