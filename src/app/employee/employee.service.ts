import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Schema as MongooseSchema } from 'mongoose';
import { Employee, EmployeeFilter } from './employee.model';
import {
  CreateEmployeeInput,
  ListEmployeeInput,
  UpdateEmployeeInput,
} from './employee.inputs';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectModel(Employee.name) private employeeModel: Model<Employee>,
  ) {}
  create(payload: CreateEmployeeInput) {
    const createdEmployee = new this.employeeModel(payload);
    return createdEmployee.save();
  }
  getById(_id: string) {
    return this.employeeModel.findById(_id).lean();
  }
  list(filters: FilterQuery<EmployeeFilter>) {
    return this.employeeModel.find(filters).lean();
  }
  update(payload: UpdateEmployeeInput) {
    return this.employeeModel
      .findByIdAndUpdate(payload._id, {}, { new: true })
      .lean();
  }
  delete(_id: string) {
    return this.employeeModel.findByIdAndDelete(_id).lean();
  }
}
