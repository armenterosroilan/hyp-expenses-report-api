import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Employee } from './employee.model';
import {
  CreateEmployeeInput,
  UpdateEmployeeInput,
  ListEmployeeInput,
} from './employee.inputs';
import { EmployeeService } from './employee.service';

@Resolver(() => Employee)
export class EmployeeResolver {
  constructor(private employeeService: EmployeeService) {}

  @Query(() => Employee)
  async employee(@Args('_id', { type: () => String }) _id: string) {
    return this.employeeService.getById(_id);
  }

  @Query(() => [Employee])
  async employees(
    @Args('filters', { nullable: true }) filters: ListEmployeeInput = {},
  ) {
    return this.employeeService.list(filters);
  }

  @Mutation(() => Employee)
  async createEmployee(@Args('payload') payload: CreateEmployeeInput) {
    return this.employeeService.create(payload);
  }

  @Mutation(() => Employee)
  async updateEmployee(@Args('payload') payload: UpdateEmployeeInput) {
    return this.employeeService.update(payload);
  }

  @Mutation(() => Employee)
  async deleteEmployee(@Args('_id', { type: () => String }) _id: string) {
    return this.employeeService.delete(_id);
  }
}
