import { Document, Schema as MongooseSchema } from 'mongoose';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Expenditure } from '../expenditure/expenditure.model';

@ObjectType()
@Schema()
export class Employee extends Document {
  @Field(() => ID)
  _id: string;

  @Field()
  @Prop()
  name: string;

  @Field()
  @Prop()
  position: string;

  @Field()
  @Prop()
  department: string;

  @Field()
  @Prop()
  manager: string;
}

export type EmployeeFilter = {
  _id: string;
  name: string;
};
export const EmployeeSchema = SchemaFactory.createForClass(Employee);
