import { FilterQuery, Schema as MongooseSchema } from 'mongoose';
import { Field, InputType } from '@nestjs/graphql';
import { Employee } from './employee.model';

@InputType()
export class CreateEmployeeInput {
  @Field(() => String)
  name: string;
  @Field(() => String)
  position: string;
  @Field(() => String)
  department: string;
  @Field(() => String)
  manager: string;
}

@InputType()
export class ListEmployeeInput {
  @Field(() => String, { nullable: true })
  _id?: string;
  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => String, { nullable: true })
  position?: string;
  @Field(() => String, { nullable: true })
  department?: string;
  @Field(() => String, { nullable: true })
  manager?: string;
}

@InputType()
export class UpdateEmployeeInput {
  @Field(() => String)
  _id: string;

  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => String, { nullable: true })
  position?: string;
  @Field(() => String, { nullable: true })
  department?: string;
  @Field(() => String, { nullable: true })
  manager?: string;
}
